package main;

import controlador.crltregistro;
import modelo.URegistro;
import modelo.Usuario;
import vista.frmRegistro;

public class PPNativo20 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        frmRegistro vistareg = new frmRegistro();
        URegistro modelregi = new URegistro();
        Usuario us = new Usuario();

        crltregistro crtl = new crltregistro(vistareg, modelregi, us);
         crtl.Iniciar();
         vistareg.setVisible(true);
    }

}

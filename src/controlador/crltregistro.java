/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import modelo.URegistro;
import modelo.Usuario;
import vista.frmRegistro;

/**
 *
 * @author Tadeo
 */
public class crltregistro implements ActionListener {

    frmRegistro vistareg = new frmRegistro();
    URegistro modelregi = new URegistro();
    Usuario us = new Usuario();

    public crltregistro(frmRegistro vistareg, URegistro modelregi, Usuario us) {
        this.vistareg = vistareg;
        this.modelregi = modelregi;
        this.us = us;
        vistareg.btnGuardar.addActionListener(this);
        vistareg.btnModificar.addActionListener(this);
        vistareg.btnLimpiar.addActionListener(this);
        vistareg.btnEliminar.addActionListener(this);
    }

    public void Iniciar() {
        this.vistareg.setLocationRelativeTo(null);
        this.vistareg.setTitle("REGISTRO DE USUARIO");

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistareg.btnGuardar) {
            us.setIdlic(Integer.parseInt(vistareg.txtId.getText()));
            us.setApellidopa(vistareg.txtapepaterno.getText());
            us.setApellidoma(vistareg.txtapematerno.getText());
            us.setNombre(vistareg.txtNombre.getText());
            us.setDni(Integer.parseInt(vistareg.txtdni.getText()));
            us.setEstado(String.valueOf(vistareg.cboestado.getSelectedItem()));
            SimpleDateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
            us.setFecha(fecha.format(vistareg.jdfecha.getDate()));
            us.setRuc(Integer.parseInt(vistareg.txtruc.getText()));
            us.setDomicilio(vistareg.txtdomicilio.getText());
            us.setDistrito(vistareg.txtdistrito.getText());
            us.setProvincia(vistareg.txtprovincia.getText());
            us.setRegion(vistareg.txtregion.getText());
            us.setEmail(vistareg.txtemail.getText());
            us.setTelefono(Integer.parseInt(vistareg.txttelefono.getText()));
            us.setCelular(Integer.parseInt(vistareg.txtcelular.getText()));
            us.setUniversidad(vistareg.txtuniversidad.getText());
            us.setResolucion(Integer.parseInt(vistareg.txtresolucion.getText()));
            us.setDiploma(Integer.parseInt(vistareg.txtdiploma.getText()));
            us.setGrados(vistareg.txtgrado.getText());
            us.setCentro(vistareg.txttrabajo.getText());
            us.setCargo(vistareg.txtcargo.getText());
            FileInputStream archivoFoto;
            /* archivoFoto = new FileInputStream(vistareg.jlfoto.getInputStream()));
             us.setFoto(vistareg.jlfoto.setIcon(null));*/
            us.setRuta(vistareg.txtruta.getText());

            if (modelregi.registrar(us)) {
                JOptionPane.showMessageDialog(null, "DATOS REGISTRADOS");

            } else {
                JOptionPane.showMessageDialog(null, "ERROR - DATOS NO REGISTRADOS");
            }
        }
    }
}

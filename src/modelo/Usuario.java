package modelo;

import java.io.FileInputStream;
import java.sql.Date;
import javax.imageio.stream.FileImageInputStream;

public class Usuario {

    public int idlic;
    private String apellidopa;
    private String apellidoma;
    private String nombre;
    private int dni;
    private String estado;
    private String fecha;
    private int ruc;
    private String domicilio;
    private String region;
    private String provincia;
    private String distrito;
    private String email;
    private int telefono;
    private int celular;
    private String universidad;
    private int resolucion;
    private int diploma;
    private String grados;
    private String centro;
    private String cargo;
    
    private FileInputStream foto;
    private String ruta;

    public Usuario() {
    }

    public Usuario(int idlic, String apellidopa, String apellidoma, String nombre, int dni, String estado, String fecha, int ruc, String domicilio, String region, String provincia, String distrito, String email, int telefono, int celular, String universidad, int resolucion, int diploma, String grados, String centro, String cargo, FileInputStream foto, int logn_foto) {
        this.idlic = idlic;
        this.apellidopa = apellidopa;
        this.apellidoma = apellidoma;
        this.nombre = nombre;
        this.dni = dni;
        this.estado = estado;
        this.fecha = fecha;
        this.ruc = ruc;
        this.domicilio = domicilio;
        this.region = region;
        this.provincia = provincia;
        this.distrito = distrito;
        this.email = email;
        this.telefono = telefono;
        this.celular = celular;
        this.universidad = universidad;
        this.resolucion = resolucion;
        this.diploma = diploma;
        this.grados = grados;
        this.centro = centro;
        this.cargo = cargo;
        this.foto = foto;
        this.ruta = ruta;
        
    }
     
        
    public int getIdlic() {
        return idlic;
    }

    public void setIdlic(int idlic) {
        this.idlic = idlic;
    }

    public String getApellidopa() {
        return apellidopa;
    }

    public void setApellidopa(String apellidopa) {
        this.apellidopa = apellidopa;
    }

    public String getApellidoma() {
        return apellidoma;
    }

    public void setApellidoma(String apellidoma) {
        this.apellidoma = apellidoma;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getRuc() {
        return ruc;
    }

    public void setRuc(int ruc) {
        this.ruc = ruc;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public int getResolucion() {
        return resolucion;
    }

    public void setResolucion(int resolucion) {
        this.resolucion = resolucion;
    }

    public int getDiploma() {
        return diploma;
    }

    public void setDiploma(int diploma) {
        this.diploma = diploma;
    }

    public String getGrados() {
        return grados;
    }

    public void setGrados(String grados) {
        this.grados = grados;
    }

    public String getCentro() {
        return centro;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public FileInputStream getFoto() {
        return foto;
    }

    public void setFoto(FileInputStream foto) {
        this.foto = foto;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public void setBinaryStream(int i, FileInputStream archivoFoto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
  

}

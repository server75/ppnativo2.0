package modelo;

import Conexion.conectar;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class URegistro extends conectar {

    public boolean registrar(Usuario user) {
        PreparedStatement psd = null;
        conectar cc = new conectar();
        Connection cn = cc.conexion();
        String sql = "INSERT INTO licenciados (idlic,apellidospaterno,apellidomaterno,nombreslic,dni,estado_civil,fecha_nacimiento,ruc,domicilio,distrito,provincia,region,email,telefonofijo,celular,universidad,nresolución,ndiploma,otrosgrados,centrotrabajo,cargoactual,foto,ruta) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            psd = cn.prepareStatement(sql);
            psd.setInt(1, user.getIdlic());
            psd.setString(2, user.getApellidopa());
            psd.setString(3, user.getApellidoma());
            psd.setString(4, user.getNombre());
            psd.setInt(5, user.getDni());
            psd.setString(6, user.getEstado());
            psd.setString(7, user.getFecha());
            psd.setInt(8, user.getRuc());
            psd.setString(9, user.getDomicilio());
            psd.setString(10, user.getDistrito());
            psd.setString(11, user.getProvincia());
            psd.setString(12, user.getRegion());
            psd.setString(13, user.getEmail());
            psd.setInt(14, user.getTelefono());
            psd.setInt(15, user.getCelular());
            psd.setString(16, user.getUniversidad());
            psd.setInt(17, user.getResolucion());
            psd.setInt(18, user.getDiploma());
            psd.setString(19, user.getGrados());
            psd.setString(20, user.getCentro());
            psd.setString(21, user.getCargo());
            psd.setBinaryStream(22, user.getFoto());
            psd.setString(23,user.getRuta());
             psd.execute();
            return true;

        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                System.err.println(e);
            }

        }
    }
}
